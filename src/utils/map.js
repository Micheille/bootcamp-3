/* eslint no-extend-native: ["error", { "exceptions": ["Array"] }] */

/**
 * `map<T, V>(callback: (element: T) => V, collection: T[]): V[]`
 *
 * Creates a new array based on the passed one by applying the callback function to each of the elements.
 * @template T
 * @template V
 * @param {function(T): V} callback - callback function applied to an element
 * @param {T[]} collection - array of the elements
 * @returns {V[]} an array of elements modified
 */

const reduceMap = (callback, collection) => {
  return collection.reduce((acc, currentValue) => {
    return [...acc, callback(currentValue)];
  }, []);
};

/**
 * Uncomment code below to test
 */

// const initialArray = [1, 2, 3, 4];

// const square = (number) => {
//   return number * number;
// };

// const mappedArray = reduceMap(square, initialArray);

// console.log(`Initial array: ${initialArray}`);
// console.log(`Squared array: ${mappedArray}`);
