/* eslint no-extend-native: ["error", { "exceptions": ["Array"] }] */

/**
 * `filter<T>(predicate: (element: T) => boolean, collection: T[]): T[]`
 *
 * Creates a new array based on the passed one by uncluding only those elements for which predicate function is true.
 * @template T
 * @param {function(T): boolean} predicate - predicate function applied to an element
 * @param {T[]} collection - array of the elements
 * @returns {T[]} an array of elements filtered out
 */

const reduceFilter = (predicate, collection) => {
  return collection.reduce((acc, currentValue) => {
    return predicate(currentValue) ? [...acc, currentValue] : [...acc];
  }, []);
};

/**
 * Uncomment code below to test
 */

// const initialArray = [1, 2, 3, 4];

// const isEven = (number) => {
//   return !(number % 2);
// };

// const filteredArray = reduceFilter(isEven, initialArray);

// console.log(`Initial array: ${initialArray}`);
// console.log(`Filtered by even numbers: ${filteredArray}`);
