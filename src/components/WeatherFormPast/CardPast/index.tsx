import React from 'react';

import './style.scss';

const monthsAbbreviated: { [key: number]: any } = {
  1: 'jan',
  2: 'feb',
  3: 'mar',
  4: 'apr',
  5: 'may',
  6: 'jun',
  7: 'jul',
  8: 'aug',
  9: 'sep',
  10: 'oct',
  11: 'nov',
  12: 'dec',
};

export default function CardPast({
  date,
  icon,
  temperatureKelvin,
}: {
  date: string;
  icon: string;
  temperatureKelvin: number;
}) {
  const dateD = new Date(date);

  const month = monthsAbbreviated[dateD.getMonth()];
  const day = dateD.getDate();
  const year = dateD.getFullYear();
  const temperatureCelsius = temperatureKelvin - 273;

  return (
    <section className="card-past">
      <p className="card-past__date">{`${day} ${month} ${year}`}</p>

      <div className="card-past__icon">
        <img src={`http://openweathermap.org/img/wn/${icon}@2x.png`} />
      </div>

      <p className="card-past__temperature">{`+ ${Math.round(
        temperatureCelsius
      )}`}</p>
    </section>
  );
}
