import React, { useEffect, useState } from 'react';

import CardPast from './CardPast';
import Intro from '../Intro';

import './style.scss';

type City = {
  lat: number;
  lon: number;
};

const cities: { [key: string]: City } = {
  Samara: {
    lat: 53.195873,
    lon: 50.100193,
  },
  Tolyatti: {
    lat: 53.507836,
    lon: 49.420393,
  },
  Saratov: {
    lat: 51.533557,
    lon: 46.034257,
  },
  Kazan: {
    lat: 55.796127,
    lon: 49.106405,
  },
  Krasnodar: {
    lat: 55.796127,
    lon: 38.975313,
  },
};

export default function WeatherFormPast() {
  const [city, setCity] = useState<string>('');
  const [date, setDate] = useState<string>('');
  const [time, setTime] = useState<number>(0);
  const [isLoaded, setLoaded] = useState<boolean>(false);
  const [error, setError] = useState<string>('');
  const [icon, setIcon] = useState<string>('');
  const [temperatureKelvin, setTemperatureKelvin] = useState<number>(0);

  const getHistoricalWeather = async (
    lat: number,
    lon: number,
    time: number
  ) => {
    const response = await fetch(
      `https://api.openweathermap.org/data/2.5/onecall/timemachine?lat=${lat}&lon=${lon}&dt=${time}&appid=f4ea689bc636fcf4c6e9a08ea5f9f7f8`
    );
    const historicalWeatherData = await response.json();

    if (!response.ok) {
      setError(historicalWeatherData.message);
      setLoaded(false);
    } else {
      setIcon(historicalWeatherData.current.weather[0].icon);
      setTemperatureKelvin(historicalWeatherData.current.temp);
      setLoaded(true);
    }
  };

  useEffect(() => {
    city &&
      time &&
      getHistoricalWeather(cities[city].lat, cities[city].lon, time);
  });

  const handleDateChange = (event: React.ChangeEvent<HTMLDataElement>) => {
    setDate(event.target.value);

    const date = new Date(event.target.value);
    const timeMilliseconds = date.getTime();
    const timeSeconds = Math.floor(timeMilliseconds / 1000);
    setTime(timeSeconds);
  };

  return (
    <article className="weather-form-past">
      <section className="weather-form-past__top">
        <h1 className="weather-form-past__title">
          Forecast for a Date in the Past
        </h1>

        <section className="weather-form-past__forms">
          <select
            className="weather-form-past__select"
            onChange={(event) => setCity(event.target.value)}
            defaultValue=""
          >
            <option value="" disabled hidden>
              Select city
            </option>
            <option value="Samara">Самара</option>
            <option value="Tolyatti">Тольятти</option>
            <option value="Saratov">Саратов</option>
            <option value="Kazan">Казань</option>
            <option value="Krasnodar">Краснодар</option>
          </select>

          <input
            className="weather-form-past__select"
            type="date"
            onChange={handleDateChange}
          />
        </section>
      </section>

      <section className="weather-form-past__content">
        {isLoaded ? (
          <CardPast
            date={date}
            icon={icon}
            temperatureKelvin={temperatureKelvin}
          />
        ) : (
          <Intro />
        )}
        <p className="weather-form-past__error">
          {!isLoaded && error && `Sorry, ${error}!`}
        </p>
      </section>
    </article>
  );
}
