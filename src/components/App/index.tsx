import React from 'react';

import WeatherApp from '../WeatherApp';

import './App.scss';

function App() {
  return (
    <div className="App">
      <WeatherApp />
    </div>
  );
}

export default App;
