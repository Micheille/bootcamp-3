import React from 'react';

import WeatherFormPast from '../WeatherFormPast';

import './style.scss';

export default function WeatherApp() {
  return (
    <div className="weather-app">
      <header className="weather-app__header">
        <div className="weather-app__title">
          <h1 className="weather-app__title-word weather-app__title-word_weather-app">
            Weather
          </h1>
          <h1 className="weather-app__title-word weather-app__title-word_forecast">
            forecast
          </h1>
        </div>
      </header>

      <main className="weather-app__main">
        <WeatherFormPast />
      </main>

      <footer className="weather-app__footer">
        <p className="weather-app__meta">С любовью от Mercury Development</p>
      </footer>
    </div>
  );
}
